# UI Library Starter

Example Vue app using the Sanger Surveillance Operations UI Library components.

## Requirements

- NodeJS 12+

## Get started

- Fork the repo, or choose it as a template when creating a new project in Gitlab
- Clone the repo
- Install dependencies `npm install`
- Start the dev environment `npm start`
- Click on the URL shown in the console to view the demo site
- Refer to the [UI Library documentation](https://malariagen.gitlab.io/conflux/ui-library/)
- Create your own Vue components

## Build for production

    npm run build
