import Vue from 'vue'
import SangerComponents from '@sanger-surveillance-operations/ui-library'
import '@sanger-surveillance-operations/ui-library/dist/ui-library.css'

import App from './App.vue'

Vue.use(SangerComponents)

new Vue({
  render: h => h(App),
  el: '#app'
})
